# Player.gd
extends KinematicBody2D

const GRAVITY = 10
const WALK_FORCE = 25
const MAX_WALK_SPEED = 100
const RUN_FORCE = 50
const MAX_RUN_SPEED = 200
const JUMP_FORCE = 300

var motion = Vector2()

onready var raycast_right = get_node("RayCastRight")
onready var raycast_left = get_node("RayCastLeft")
onready var sprite = get_node("Sprite")

onready var animation_player = get_node("AnimationPlayer")
var animation = ""

func _ready():
	raycast_right.add_exception(self)
	raycast_left.add_exception(self)
	set_process(true)

func _process(delta):
	var on_ground = raycast_right.is_colliding() || raycast_left.is_colliding()
	var horizontal_input = Input.is_action_pressed("ui_right") - Input.is_action_pressed("ui_left")
	var running = Input.is_action_pressed("ui_select")
	
	horizontal_movement(horizontal_input, running, on_ground)
	vertical_movement(horizontal_input, on_ground)
	
	move(Vector2(motion.x*delta, 0))
	move(Vector2(0, motion.y*delta))

func horizontal_movement(horizontal_input, running, on_ground):
	if horizontal_input != 0:
		if running:
			motion.x += horizontal_input*RUN_FORCE
			motion.x = clamp(motion.x, -MAX_RUN_SPEED, MAX_RUN_SPEED)
			set_animation("Run")
		else:
			motion.x += horizontal_input*WALK_FORCE
			motion.x = clamp(motion.x, -MAX_WALK_SPEED, MAX_WALK_SPEED)
			set_animation("Walk")
		sprite.set_flip_h(!(horizontal_input+1)/2)
	else:
		if on_ground:
			motion.x = lerp(motion.x, 0, .2)
			set_animation("Idle")
		else:
			motion.x = lerp(motion.x, 0, .02)

func vertical_movement(horizontal_input, on_ground):
	if !on_ground:
		motion.y += GRAVITY
		if motion.y < 0:
			set_animation("Jump")
		else:
			set_animation("Fall")
	else:
		motion.y = 0
		if Input.is_action_pressed("ui_up"):
			motion.y -= JUMP_FORCE

func set_animation(new_animation):
	if new_animation != animation:
		animation_player.play(new_animation)
		animation = new_animation