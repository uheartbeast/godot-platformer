#tool

# Autotile.gd
extends TileMap

# Direction enum (for autotile math)
enum {
	NORTH_WEST = 1,
	NORTH = 2,
	NORTH_EAST = 4,
	WEST = 8,
	EAST = 16,
	SOUTH_WEST = 32,
	SOUTH = 64,
	SOUTH_EAST = 128
}

var tile_data = {2:1, 8:2, 10:3, 11:4, 16:5, 18:6, 22:7, 24:8, 26:9, 27:10, 30:11, 31:12, 64:13, 66:14, 72:15, 74:16, 75:17, 80:18, 82:19, 86:20, 88:21, 90:22, 91:23, 94:24, 95:25, 104:26, 106:27, 107:28, 120:29, 122:30, 123:31, 126:32, 127:33, 208:34, 210:35, 214:36, 216:37, 218:38, 219:39, 222:40, 223:41, 248:42, 250:43, 251:44, 254:45, 255:46, 0:47}

func _ready():
	update_tiles(get_used_cells())
	if get_tree().is_editor_hint():
		set_process(true)

func _process(delta):
	update_tiles(get_used_cells())

func update_tiles(cell_list):
	for cell in cell_list:
		var north_cell = (get_cell(cell.x, cell.y-1) != -1)*NORTH
		var west_cell = (get_cell(cell.x-1, cell.y) != -1)*WEST
		var east_cell = (get_cell(cell.x+1, cell.y) != -1)*EAST
		var south_cell = (get_cell(cell.x, cell.y+1) != -1)*SOUTH
		
		var north_west_cell = (get_cell(cell.x-1, cell.y-1) != -1 && north_cell && west_cell)*NORTH_WEST
		var north_east_cell = (get_cell(cell.x+1, cell.y-1) != -1 && north_cell && east_cell)*NORTH_EAST
		var south_west_cell = (get_cell(cell.x-1, cell.y+1) != -1 && south_cell && west_cell)*SOUTH_WEST
		var south_east_cell = (get_cell(cell.x+1, cell.y+1) != -1 && south_cell && east_cell)*SOUTH_EAST
		
		var cell_index = north_west_cell + north_cell + north_east_cell + west_cell + east_cell + south_west_cell + south_cell + south_east_cell
		set_cell(cell.x, cell.y, tile_data[cell_index])
